﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class GruposAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GruposAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<Grupos> GetGrupos(string filtro)
        {
            var listGrupos = new List<Grupos>();
            var ds = new DataSet();
            string consulta = "Select * from grupos2 where carrera like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupos2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupos = new Grupos
                {
                    Idgrupos = Convert.ToInt32(row["idgrupo"]),
                    Nombregrupo = row["nombregrupo"].ToString(),
                    Carrera = row["carrera"].ToString()
                };
                listGrupos.Add(grupos);
            }
            return listGrupos;
        }
    }
}
