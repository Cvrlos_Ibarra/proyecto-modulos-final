﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class AsignacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AsignacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Asignacion asignacion)
        {
            if (asignacion.Idasignacion == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into asignacion values(null,'{0}','{1}','{2}') ",
                    asignacion.Fkprofesor, asignacion.Fkmateria,asignacion.Fkgrupos);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update asignacion set fkprofesor = '{0}',fkmateria = '{1}',fkgrupos = '{2}' where idasignacion = {3}", asignacion.Fkprofesor, asignacion.Fkmateria,asignacion.Fkgrupos, asignacion.Idasignacion);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idasignacion)
        {
            //eliminar
            string consulta = string.Format("Delete from asignacion where idasignacion = '{0}'", idasignacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Asignacion> GetAsignacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAsignacion = new List<Asignacion>();
            var ds = new DataSet();
            string consulta = "Select * from asignacion where fkgrupos like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "asignacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asignacion = new Asignacion
                {
                    Idasignacion = Convert.ToInt32(row["idasignacion"]),
                    Fkprofesor = row["fkprofesor"].ToString(),
                    Fkmateria = row["fkmateria"].ToString(),
                    Fkgrupos = Convert.ToInt32(row["fkgrupos"])

                };
                listAsignacion.Add(asignacion);
            }
            return listAsignacion;
        }



        

        public DataSet Consulta(int filtro)
        {
            var ds = new DataSet();
            string consulta = "Select * from asignacion where fkprofesor = '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "asignacion");

            return ds;
        }
    }
}
