﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class CalificacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CalificacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Calificacion calificacion)
        {
            if (calificacion.Idcalificacion == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into calificacion values(null,'{0}','{1}','{2}','{3}','{4}','{5}') ",
                    calificacion.Fkalumno, calificacion.Materia, calificacion.Parcial1,calificacion.Parcial2,calificacion.Parcial3,calificacion.Parcial4);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update calificacion set fkalumno = '{0}',materia = '{1}',parcial1 = '{2}',parcial2 = '{3}',parcial3 = '{4}',parcial4 = '{5}' where idcalificacion = {6}", calificacion.Fkalumno, calificacion.Materia, calificacion.Parcial1, calificacion.Parcial2, calificacion.Parcial3, calificacion.Parcial4,calificacion.Idcalificacion);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idcalificacion)
        {
            //eliminar
            string consulta = string.Format("Delete from calificacion where idcalificacion = '{0}'", idcalificacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Calificacion> GetCalificacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listCalificacion = new List<Calificacion>();
            var ds = new DataSet();
            string consulta = "Select * from calificacion where fkgrupo like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "calificacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var calificacion = new Calificacion
                {
                    Idcalificacion = Convert.ToInt32(row["idcalificacion"]),
                    Fkalumno = Convert.ToInt32(row["fkalumno"]),
                    Materia = row["materia"].ToString(),
                    Parcial1 = Convert.ToInt32(row["parcial1"]),
                    Parcial2 = Convert.ToInt32(row["parcial2"]),
                    Parcial3 = Convert.ToInt32(row["parcial3"]),
                    Parcial4 = Convert.ToInt32(row["parcial4"])


                };
                listCalificacion.Add(calificacion);
            }
            return listCalificacion;
        }





        public DataSet Consulta(int filtro)
        {
            var ds = new DataSet();
            string consulta = "Select * from asignacion where fkprofesor = '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "asignacion");

            return ds;
        }
    }
}
