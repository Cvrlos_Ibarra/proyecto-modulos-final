﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Grupo grupo)
        {
            if (grupo.Idg == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into grupo values(null,'{0}','{1}') ",
                    grupo.Alumno, grupo.Grupito);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update grupo set fkalumno = '{0}',fkgrupo = '{1}' where idg = {2}", grupo.Alumno, grupo.Grupito, grupo.Idg);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idgrupo)
        {
            //eliminar
            string consulta = string.Format("Delete from grupo where idg = '{0}'", idgrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupo> GetGrupo(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupo = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "Select * from grupo where fkgrupo like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupo");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupo
                {
                    Idg = Convert.ToInt32(row["idg"]),
                    Alumno = Convert.ToInt32(row["fkalumno"]),
                    Grupito = Convert.ToInt32(row["fkgrupo"])

                };
                listGrupo.Add(grupo);
            }
            return listGrupo;
        }



        public List<Alumno> GetAlumnos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listalumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select * from alumno where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "alumno");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumno
                {
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanacimiento = row["fechanacimiento"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString()

                };
                listalumno.Add(alumno);
            }
            return listalumno;
        }

        public DataSet Consulta(int filtro)
        {
            var ds = new DataSet();
            string consulta = "Select * from grupo where fkalumno = '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupo");

            return ds;
        }

    }
}
