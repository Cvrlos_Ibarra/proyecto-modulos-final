﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaAsignacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VistaAsignacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaAsignacion> GetAsignacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvistaasignacion = new List<VistaAsignacion>();
            var ds = new DataSet();
            string consulta = "Select * from v_asignacion2 where profesor like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_asignacion2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asignacion = new VistaAsignacion
                {


                    Id = Convert.ToInt32(row["id"]),
                    Grupo = row["grupo"].ToString(),
                    Carrera = row["carrera"].ToString(),
                    Materia = row["materia"].ToString(),
                    Numerocontrol = row["numerocontrol"].ToString(),
                    Profesor = row["profesor"].ToString(),
                    App = row["app"].ToString(),
                    Apm = row["apm"].ToString()
                    
                    





                };
                listvistaasignacion.Add(asignacion);
            }
            return listvistaasignacion;
        }
    }
}
