﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaCalificacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VistaCalificacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaCalificacion> GetCalificacion(string filtro,string filtro2)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvistacalificacion = new List<VistaCalificacion>();
            var ds = new DataSet();
            string consulta = "Select * from v_cali where carrera like '%" + filtro + "%' and grupo like '%" + filtro2 + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_cali");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var calificacion = new VistaCalificacion
                {


                    
                   
                    Carrera = row["carrera"].ToString(),
                    Grupo = row["grupo"].ToString(),
                    Materia = row["materia"].ToString(),
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    App = row["app"].ToString(),
                    Apm = row["apm"].ToString(),
                    Parcial1 = Convert.ToInt32(row["parcial1"]),
                    Parcial2 = Convert.ToInt32(row["parcial2"]),
                    Parcial3 = Convert.ToInt32(row["parcial3"]),
                    Parcial4 = Convert.ToInt32(row["parcial4"]),
                    Id = Convert.ToInt32(row["id"]),
                    Idgrupo = Convert.ToInt32(row["idgrupo"]),







                };
                listvistacalificacion.Add(calificacion);
            }
            return listvistacalificacion;
        }
    }
}
