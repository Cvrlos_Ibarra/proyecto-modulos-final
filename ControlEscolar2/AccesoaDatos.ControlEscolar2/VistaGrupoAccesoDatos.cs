﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaGrupoAccesoDatos
    {

        ConexionAccesoDatos conexion;

        public VistaGrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaGrupo> GetGrupos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvistagrupo = new List<VistaGrupo>();
            var ds = new DataSet();
            string consulta = "Select * from v_grupos2 where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_grupos2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new VistaGrupo
                {


                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    App = row["app"].ToString(),
                    Apm = row["apm"].ToString(),
                    Grupo = row["grupo"].ToString(),
                    Carrera = row["carrera"].ToString(),
                    Id = row["id"].ToString()
                   




                };
                listvistagrupo.Add(grupo);
            }
            return listvistagrupo;
        }

    }
}
