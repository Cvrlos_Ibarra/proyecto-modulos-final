﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Escuela escuela)
        {
            if (escuela.Id == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into escuela values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}') ",
                   escuela.Nombre, escuela.Rfc, escuela.Domicilio, escuela.Telefono, escuela.Email, escuela.Pagina, escuela.Director, escuela.Logo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update escuela set nombre = '{0}',rfc = '{1}' ,domicilio = '{2}',telefono = '{3}' ,email = '{4}',pagina = '{5}' ,director = '{6}',logo = '{7}' where id = '{8}'",
                escuela.Nombre, escuela.Rfc, escuela.Domicilio, escuela.Telefono, escuela.Email, escuela.Pagina, escuela.Director, escuela.Logo, escuela.Id);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            //eliminar
            string consulta = string.Format("Delete from escuela where id = '{0}'", id);
            conexion.EjecutarConsulta(consulta);
        }
        public DataSet Consulta()
        {
            var ds = new DataSet();
            string consulta = "Select * from escuela";

            ds = conexion.ObtenerDatos(consulta, "escuela");

            return ds;
        }


        public List<Escuela> GetEscuela(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listescuela = new List<Escuela>();
            var ds = new DataSet();
            string consulta = "Select * from escuela where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {

                    Id = Convert.ToInt32(row["id"]),
                    Nombre = row["nombre"].ToString(),
                    Rfc = row["rfc"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Telefono = Convert.ToInt32(row["telefono"]),
                    Email = row["email"].ToString(),
                    Pagina = row["pagina"].ToString(),
                    Director = row["director"].ToString(),
                    Logo = row["logo"].ToString(),




                };
                listescuela.Add(escuela);
            }
            return listescuela;
        }
    }
}
