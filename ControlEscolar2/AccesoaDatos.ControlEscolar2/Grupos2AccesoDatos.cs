﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class Grupos2AccesoDatos
    {
        ConexionAccesoDatos conexion;

        public Grupos2AccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Grupos2 grupo)
        {
            if (grupo.Idgrupo2 == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into grupos2 values(null,'{0}','{1}') ",
                    grupo.Nombregrupo, grupo.Carrera);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update grupos2 set idgrupo = '{0}',nombregrupo = '{1}' where idgrupo = {2}", grupo.Idgrupo2, grupo.Nombregrupo, grupo.Carrera);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idgrupo)
        {
            //eliminar
            string consulta = string.Format("Delete from grupos2 where idgrupo = '{0}'", idgrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupos2> GetGrupo(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupo = new List<Grupos2>();
            var ds = new DataSet();
            string consulta = "Select * from grupos2 where nombregrupo like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "grupos2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupos2
                {
                    Idgrupo2 = Convert.ToInt32(row["idgrupo"]),
                    Nombregrupo = row["nombregrupo"].ToString(),
                    Carrera = row["carrera"].ToString()

                };
                listGrupo.Add(grupo);
            }
            return listGrupo;
        }



        
    }
}
