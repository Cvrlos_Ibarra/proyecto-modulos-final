﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class EstudioAccesoDatos
    {

        ConexionAccesoDatos conexion;

        public EstudioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Estudios estudios)
        {
            if (estudios.Codigoest == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into estudios values(null,'{0}','{1}','{2}') ",
                   estudios.Estudio, estudios.Documento, estudios.Fkprofesor);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update estudios set estudio = '{0}',documento = '{1}' ,fkprofesor = '{2}' where codigoestudio = '{3}'",
                     estudios.Estudio,estudios.Documento, estudios.Fkprofesor, estudios.Codigoest);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int codigoestudio)
        {
            //eliminar
            string consulta = string.Format("Delete from estudios where codigoestudio = '{0}'", codigoestudio);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listestudios = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select * from estudios where estudio like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Estudios
                {
                    
                    Codigoest = Convert.ToInt32(row["codigoestudio"]),
                    Estudio = row["estudio"].ToString(),
                    Documento = row["documento"].ToString(),
                    Fkprofesor = row["fkprofesor"].ToString()




                };
                listestudios.Add(estudios);
            }
            return listestudios;
        }

    }
}
