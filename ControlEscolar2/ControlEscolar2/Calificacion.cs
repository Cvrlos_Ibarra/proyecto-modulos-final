﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using Microsoft.Office.Interop.Excel;
using System.Drawing.Printing;
using System.IO;

namespace ControlEscolar2
{
    public partial class Calificacion : Form
    {
        private VistaGrupoManejador _vistagrupoManejador;
        private MateriasManejador _materiasManejador;
        private VistaCalificacionManejador _vistaCalificacionManejador;
        private CalificacionManejador _calificacionManejador;
        private GruposManejador _gruposManejador;
        public Calificacion()
        {
            InitializeComponent();
            _vistagrupoManejador = new VistaGrupoManejador();
            _materiasManejador = new MateriasManejador();
            _vistaCalificacionManejador = new VistaCalificacionManejador();
            _calificacionManejador = new CalificacionManejador();
            _gruposManejador = new GruposManejador();
        }

        private void Calificacion_Load(object sender, EventArgs e)
        {
            MostrarMaterias("");
            BuscarCalificacion("","");
            BuscarGrupo("");
            dgvGrupo.Visible = false;
        }

        private void MostrarMaterias(string filtro)
        {
            cmbMateria.DataSource = _materiasManejador.GetMaterias(filtro);


            cmbMateria.ValueMember = "idmateria";
            cmbMateria.DisplayMember = "nombremateria";
        }

        private void BuscarGrupo(string filtro)
        {
            //dgvGrupo.DataSource = _grupomanejador.GetGrupo(filtro);
            dgvGrupo.DataSource = _vistagrupoManejador.GetGrupos(filtro);
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            //btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }

        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApp.Enabled = activar;
            txtApm.Enabled = activar;

            cmbMateria.Enabled = activar;
           
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApp.Text = "";
            txtApm.Text = "";
            txtNumero.Text = "";
            lblId.Text = "0";
            lblIdGrupo.Text = "";
            txtCarrera.Text = "";
            txtGrupo.Text = "";
            txt1.Text = "";
            txt2.Text = "";
            txt3.Text = "";
            txt4.Text = "";
            cmbMateria.Text = "";
        }

        private void BuscarCalificacion(string filtro, string filtro2)
        {
           
            dgvCalificacion.DataSource = _vistaCalificacionManejador.GetCalificacion(filtro,filtro2);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            //ControlarBotones(false, true, true, false);
            //ControlarCuadros(true);
            dgvGrupo.Visible = true;
            
        }

        private void GuardarCalificacion()
        {
            _calificacionManejador.Guardar(new Entidades.ControlEscolar2.Calificacion
            {
                Idcalificacion = Convert.ToInt32(lblId.Text),
                Fkalumno = Convert.ToInt32(lblIdGrupo.Text),
                Materia = cmbMateria.Text,
                Parcial1 = Convert.ToInt32(txt1.Text),
                Parcial2 = Convert.ToInt32(txt2.Text),
                Parcial3 = Convert.ToInt32(txt3.Text),
                Parcial4 = Convert.ToInt32(txt4.Text)
            });
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarCalificacion();
            LimpiarCuadros();
            BuscarCalificacion("","");
            dgvGrupo.Visible = false;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void EliminarCalificacion()
        {
            var idcali = dgvCalificacion.CurrentRow.Cells["id"].Value;
            _calificacionManejador.Eliminar(Convert.ToInt32(idcali));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarCalificacion();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificarCalificacion()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Visible = true;
            lblId.Text = dgvCalificacion.CurrentRow.Cells["id"].Value.ToString();
            lblIdGrupo.Text = dgvCalificacion.CurrentRow.Cells["idgrupo"].Value.ToString();
            txtNumero.Text = dgvCalificacion.CurrentRow.Cells["numerocontrol"].Value.ToString();
            txtNombre.Text = dgvCalificacion.CurrentRow.Cells["nombre"].Value.ToString();
            cmbMateria.Text = dgvCalificacion.CurrentRow.Cells["materia"].Value.ToString();
            txtApp.Text = dgvCalificacion.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dgvCalificacion.CurrentRow.Cells["apm"].Value.ToString();
            txtCarrera.Text = dgvCalificacion.CurrentRow.Cells["carrera"].Value.ToString();
            txtGrupo.Text = dgvCalificacion.CurrentRow.Cells["grupo"].Value.ToString();
            txt1.Text = dgvCalificacion.CurrentRow.Cells["parcial1"].Value.ToString();
            txt2.Text = dgvCalificacion.CurrentRow.Cells["parcial2"].Value.ToString();
            txt3.Text = dgvCalificacion.CurrentRow.Cells["parcial3"].Value.ToString();
            txt4.Text = dgvCalificacion.CurrentRow.Cells["parcial4"].Value.ToString();

        }
        private void ModificarCalificacion2()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Visible = true;
            lblId.Text = "0";
            lblIdGrupo.Text = dgvGrupo.CurrentRow.Cells["id"].Value.ToString();
            txtNumero.Text = dgvGrupo.CurrentRow.Cells["numerocontrol"].Value.ToString();
            txtNombre.Text = dgvGrupo.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApp.Text = dgvGrupo.CurrentRow.Cells["App"].Value.ToString();
            txtApm.Text = dgvGrupo.CurrentRow.Cells["Apm"].Value.ToString();
            
            
            txtGrupo.Text = dgvGrupo.CurrentRow.Cells["grupo"].Value.ToString();
            txtCarrera.Text = dgvGrupo.CurrentRow.Cells["carrera"].Value.ToString();

            


        }

        private void DgvGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarCalificacion2();
                BuscarGrupo("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void DgvCalificacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarCalificacion();
                BuscarCalificacion("","");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void MostrarGrupos(string filtro)
        {
            cmbGrupo.DataSource = _gruposManejador.GetGrupos(filtro);


            cmbGrupo.ValueMember = "idgrupos";
            cmbGrupo.DisplayMember = "nombregrupo";
        }

        private void CmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarGrupos(cmbCarrera.Text);
        }

        private void CmbGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarCalificacion(cmbCarrera.Text,cmbGrupo.Text);
        }

        private void ExportarDataGridViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    // recorremos el datagridview rellenando la hoja de trabajo

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();

                        }

                    }
                    libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();

                    MessageBox.Show("Calificaciones exportadas", "Calificacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la creacion del archivo intenta otro nombre", "Error de creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvCalificacion);
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.DefaultPageSettings.Landscape = true;
            doc.PrinterSettings.PrinterName = "Microsoft Print to PDF";

            PrintPreviewDialog ppd = new PrintPreviewDialog { Document = doc };
            ((Form)ppd).WindowState = FormWindowState.Maximized;

            doc.PrintPage += delegate (object ev, PrintPageEventArgs ep)
            {
                const int DGV_ALTO = 35;
                int left = ep.MarginBounds.Left, top = ep.MarginBounds.Top;

                foreach (DataGridViewColumn col in dgvCalificacion.Columns)
                {
                    ep.Graphics.DrawString(col.HeaderText, new System.Drawing.Font("Segoe UI", 12, FontStyle.Bold), Brushes.DeepSkyBlue, left, top);
                    left += col.Width;

                    if (col.Index < dgvCalificacion.ColumnCount - 1)
                        ep.Graphics.DrawLine(Pens.Gray, left - 5, top, left - 5, top + 43 + (dgvCalificacion.RowCount - 1) * DGV_ALTO);
                }
                left = ep.MarginBounds.Left;
                ep.Graphics.FillRectangle(Brushes.Black, left, top + 40, ep.MarginBounds.Right - left, 3);
                top += 43;

                foreach (DataGridViewRow row in dgvCalificacion.Rows)
                {
                    if (row.Index == dgvCalificacion.RowCount - 1) break;
                    left = ep.MarginBounds.Left;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        ep.Graphics.DrawString(Convert.ToString(cell.Value), new System.Drawing.Font("Segoe UI", 13), Brushes.Black, left, top + 4);
                        left += cell.OwningColumn.Width;
                    }
                    top += DGV_ALTO;
                    ep.Graphics.DrawLine(Pens.Gray, ep.MarginBounds.Left, top, ep.MarginBounds.Right, top);
                }
            };
            ppd.ShowDialog();
        }
    }
}
