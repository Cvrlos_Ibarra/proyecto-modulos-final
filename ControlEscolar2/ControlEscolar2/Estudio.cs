﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using System.IO;

namespace ControlEscolar2
{
    public partial class Estudio : Form
    {
        private OpenFileDialog _imagenjpg;

        //private OpenFileDialog _archivopdf;

        private string _ruta;
        



        private EstudioManejador _estudiomanejador;
        private ProfesorManejador _profemanejador;
        private VistaEstudioManejador _vistamanejador;
        public Estudio()
        {
            InitializeComponent();

            _imagenjpg = new OpenFileDialog();
            //_archivopdf = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Titulos\\";
            //_ruta2 = Application.StartupPath + "\\Titulo_pdf\\";


            _estudiomanejador = new EstudioManejador();
            _profemanejador = new ProfesorManejador();
            _vistamanejador = new VistaEstudioManejador();
        }

        private void Estudio_Load(object sender, EventArgs e)
        {
            BuscarEstudios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            MostrarProfe("");
            lblCodigo.Text = "0";

        }
        private void MostrarProfe(string filtro)
        {
            cmbProfe.DataSource = _profemanejador.GetProfesores(filtro);
           
            cmbProfe.ValueMember = "numerocontrol";
            cmbProfe.DisplayMember = "nombre";
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnAgregar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbProfe.Enabled = activar;
            txtTitulo.Enabled = activar;
            txtDoc.Enabled = activar;


        }
        private void LimpiarCuadros()
        {
            txtDoc.Text = "";
            txtTitulo.Text = "";
            lblCodigo.Text = "";
            cmbProfe.Text = "";

        }
        private void BuscarEstudios(string filtro)
        {
            //dgvEstudios.DataSource = _estudiomanejador.GetEstudios(filtro);
            dgvEstudios.DataSource = _vistamanejador.GetEstudios(filtro);
            
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtTitulo.Focus();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            //ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
            try
            {
                GuardarEstudio();
                GuardarImagenjpg();
                LimpiarCuadros();
                BuscarEstudios("");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void GuardarEstudio()
        {
            _estudiomanejador.Guardar(new Estudios
            {
                Codigoest = int.Parse(lblCodigo.Text),
                Estudio = txtTitulo.Text,
                Documento = txtDoc.Text,
                Fkprofesor = cmbProfe.SelectedValue.ToString()


            });
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudios(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarTitulo();
                    BuscarEstudios("");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarTitulo()
        {
            string dc = dgvEstudios.CurrentRow.Cells["documento"].Value.ToString();
            var codigoEstudio = dgvEstudios.CurrentRow.Cells["clave"].Value;
            _estudiomanejador.Eliminar(Convert.ToInt32(codigoEstudio));
            EliminarArchivo(_ruta + dc);
        }

        private void DgvEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarTitulo();
                BuscarEstudios("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarTitulo()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblCodigo.Visible = true;
            

            lblCodigo.Text = dgvEstudios.CurrentRow.Cells["clave"].Value.ToString();
            txtTitulo.Text = dgvEstudios.CurrentRow.Cells["titulo"].Value.ToString();
            txtDoc.Text = dgvEstudios.CurrentRow.Cells["documento"].Value.ToString();
            cmbProfe.Text = dgvEstudios.CurrentRow.Cells["ncontrol"].Value.ToString();
            
        }

        private void BtnSubir_Click(object sender, EventArgs e)
        {
            CargarImagenjpg();
        }
        private void CargarImagenjpg()
        {

            _imagenjpg.Filter = "Imagen tipo (*.jpg)|*.jpg| archivo tipo (*.pdf)|*.pdf";
            _imagenjpg.Title = "Cargar Imagen";


            _imagenjpg.ShowDialog();

            if (_imagenjpg.FileName != "")
            {
                var archivo = new FileInfo(_imagenjpg.FileName);
                txtDoc.Text = archivo.Name;
            }
        }

        private void GuardarImagenjpg()
        {
            try
            {

                if (_imagenjpg.FileName.Contains(".jpg"))
                {
                    if (_imagenjpg.FileName != null)
                    {
                        if (_imagenjpg.FileName != "")
                        {
                            var archivo = new FileInfo(_imagenjpg.FileName);

                            if (Directory.Exists(_ruta))
                            {


                                var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                                FileInfo archivoAnterior;
                                MessageBox.Show("Registro y archivo guardados");

                                if (obtenerArchivos.Length != 0)
                                {

                                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                                    if (archivoAnterior.Exists)
                                    {

                                        archivo.CopyTo(_ruta + archivo.Name);

                                    }

                                }
                                else
                                {
                                    archivo.CopyTo(_ruta + archivo.Name);
                                }


                            }
                            else
                            {
                                Directory.CreateDirectory(_ruta);
                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }

                    }
                }
                if (_imagenjpg.FileName.Contains(".pdf"))
                {
                    if (_imagenjpg.FileName != null)
                    {
                        if (_imagenjpg.FileName != "")
                        {
                            var archivo = new FileInfo(_imagenjpg.FileName);

                            if (Directory.Exists(_ruta))
                            {


                                var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                                FileInfo archivoAnterior;
                                MessageBox.Show("Archivo Guardado");

                                if (obtenerArchivos.Length != 0)
                                {

                                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                                    if (archivoAnterior.Exists)
                                    {

                                        archivo.CopyTo(_ruta + archivo.Name);

                                    }

                                }
                                else
                                {
                                    archivo.CopyTo(_ruta + archivo.Name);
                                }

                            }
                            else
                            {
                                Directory.CreateDirectory(_ruta);
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Esta imagen ya está Guardada");
            }

            

        }
        private void EliminarArchivo(string ruta)
        {
            try
            {
                if (File.Exists(@ruta))
                {
                    File.Delete(@ruta);
                    txtDoc.Clear();
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        



        private void CmbProfe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
