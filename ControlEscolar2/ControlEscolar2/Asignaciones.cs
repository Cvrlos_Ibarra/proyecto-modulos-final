﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class Asignaciones : Form
    {
        private VistaAsignacionManejador _vistaasignacion;
        private GruposManejador _gruposManejador;
        private MateriasManejador _materiasManejador;
        private ProfesorManejador _profesorManejador;
        private AsignacionManejador _asignacionmanejador;

        public Asignaciones()
        {
            InitializeComponent();
            _gruposManejador = new GruposManejador();
            _materiasManejador = new MateriasManejador();
            _profesorManejador = new ProfesorManejador();
            _asignacionmanejador = new AsignacionManejador();
            _vistaasignacion = new VistaAsignacionManejador();
        }

        private void Asignaciones_Load(object sender, EventArgs e)
        {
            MostrarMaterias("");
            BuscarProfesores("");
            BuscarAsignacion("");
            dgvProfe.Visible = false;
            Campos();
        }
        private void MostrarGrupos(string filtro)
        {
            cmbGrupo.DataSource = _gruposManejador.GetGrupos(filtro);


            cmbGrupo.ValueMember = "idgrupos";
            cmbGrupo.DisplayMember = "nombregrupo";
        }

        private void CmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarGrupos(cmbCarrera.Text);
        }
        private void MostrarMaterias(string filtro)
        {
            cmbMateria.DataSource = _materiasManejador.GetMaterias(filtro);


            cmbMateria.ValueMember = "idmateria";
            cmbMateria.DisplayMember = "nombremateria";
        }
        private void BuscarProfesores(string filtro)
        {
            dgvProfe.DataSource = _profesorManejador.GetProfesores(filtro);
        }


        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }

        private void ControlarCuadros(bool activar)
        {
            

            cmbCarrera.Enabled = activar;
            cmbGrupo.Enabled = activar;
            cmbMateria.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApp.Text = "";
            txtApm.Text = "";

            lblId.Text = "0";
            cmbGrupo.Text = "";
            cmbCarrera.Text = "";
            cmbCarrera.Text = "";
            txtNumero.Text = "";
        }

        private void BuscarAsignacion(string filtro)
        {
            //dgvGrupo.DataSource = _grupomanejador.GetGrupo(filtro);
            //dgvAsignacion.DataSource = _asignacionmanejador.GetAsignacion(filtro);
            dgvAsignacion.DataSource = _vistaasignacion.GetAsignacion(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            
        }

        private void Campos()
        {
            lblId.Enabled = false;
            txtNumero.Enabled = false;
            txtNombre.Enabled = false;
            txtApp.Enabled = false;
            txtApm.Enabled = false;

        }


        private void GuardarAsignacion()
        {
            _asignacionmanejador.Guardar(new Entidades.ControlEscolar2.Asignacion
            {
                Idasignacion = Convert.ToInt32(lblId.Text),
                Fkprofesor = txtNumero.Text,
                Fkmateria = cmbMateria.SelectedValue.ToString(),
                Fkgrupos = Convert.ToInt32(cmbGrupo.SelectedValue)
            });
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            //ControlarBotones(true, false, false, true);
            //ControlarCuadros(false);
           
                    GuardarAsignacion();
                    LimpiarCuadros();
                    BuscarAsignacion("");
            dgvProfe.Visible = false;
                
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void EliminarAsignacion()
        {
            var idasig = dgvAsignacion.CurrentRow.Cells["id"].Value;
            _asignacionmanejador.Eliminar(Convert.ToInt32(idasig));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAsignacion();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ModificarAsignacion()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Visible = true;
            lblId.Text = dgvAsignacion.CurrentRow.Cells["id"].Value.ToString();
            txtNumero.Text = dgvAsignacion.CurrentRow.Cells["numerocontrol"].Value.ToString();
            txtNombre.Text = dgvAsignacion.CurrentRow.Cells["profesor"].Value.ToString();
            cmbMateria.Text = dgvAsignacion.CurrentRow.Cells["materia"].Value.ToString();
            txtApp.Text = dgvAsignacion.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dgvAsignacion.CurrentRow.Cells["apm"].Value.ToString();
            cmbGrupo.Text = dgvAsignacion.CurrentRow.Cells["grupo"].Value.ToString();
            cmbCarrera.Text = dgvAsignacion.CurrentRow.Cells["carrera"].Value.ToString();

        }
        private void ModificarAsignacion2()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Visible = true;
            lblId.Text = "0";
            txtNumero.Text = dgvProfe.CurrentRow.Cells["numerocontrol"].Value.ToString();
            txtNombre.Text = dgvProfe.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApp.Text = dgvProfe.CurrentRow.Cells["App"].Value.ToString();
            txtApm.Text = dgvProfe.CurrentRow.Cells["Apm"].Value.ToString();
            
            cmbGrupo.Text = "";
            cmbCarrera.Text = "";
            cmbMateria.Text = "";


        }

        private void DgvProfe_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvAsignacion_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvProfe_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAsignacion2();
                BuscarProfesores("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void DgvAsignacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAsignacion();
                BuscarAsignacion("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnNuevo_Click_1(object sender, EventArgs e)
        {
            dgvProfe.Visible = true;
        }
    }
}
