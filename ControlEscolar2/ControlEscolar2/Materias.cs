﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class Materias : Form
    {
        private MateriasManejador _materiasManejador;
        private VistaMateriasManejador _vistamanejador;


        public Materias()
        {
            InitializeComponent();

            _materiasManejador = new MateriasManejador();
            _vistamanejador = new VistaMateriasManejador();
        }

        private void Materias_Load(object sender, EventArgs e)
        {
            BuscarMateria("");
            MostrarMaterias("");

            lblx.Text = "0";
            lblx.Visible = false;
            Cuadros();
        }
        public void Cuadros()
        {
            if (cbxAnterior.Checked)
            {
                cmbAnterior.Enabled = false;
            }
            if (cbxPosterior.Checked)
            {
                cmbPosterior.Enabled = false;
            }
        }
        private void MostrarMaterias(string filtro)
        {
            cmbAnterior.DataSource = _materiasManejador.GetMaterias(filtro);
            cmbPosterior.DataSource = _materiasManejador.GetMaterias(filtro);

            cmbAnterior.ValueMember = "idmateria";
            cmbAnterior.DisplayMember = "nombremateria";

            cmbPosterior.ValueMember = "idmateria";
            cmbPosterior.DisplayMember = "nombremateria";
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbAnterior.Enabled = activar;
            cmbPosterior.Enabled = activar;
            cmbSemestre.Enabled = activar;
            cmbCarrera.Enabled = activar;
            txtId.Enabled = activar;
            txtMateria.Enabled = activar;
            txtPractica.Enabled = activar;
            txtTeoria.Enabled = activar;


        }
        private void LimpiarCuadros()
        {
            cmbAnterior.Text = "";
            cmbPosterior.Text = "";
            cmbSemestre.Text = "";
            cmbCarrera.Text = "";
            txtId.Text = "";
            txtMateria.Text = "";
            txtPractica.Text = "";
            txtTeoria.Text = "";

        }


        private void BuscarMateria(string filtro)
        {
            //dgvEstudios.DataSource = _estudiomanejador.GetEstudios(filtro);
            dgvMaterias.DataSource = _vistamanejador.GetMaterias(filtro);

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtId.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (cbxAnterior.Checked  && cbxPosterior.Checked)
            {
                try
                {
                    GuardarMateria();

                    LimpiarCuadros();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (cbxAnterior.Checked == false && cbxPosterior.Checked == false)
            {
                try
                {
                    GuardarMateria2();

                    LimpiarCuadros();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (cbxAnterior.Checked == false && cbxPosterior.Checked == true)
            {
                try
                {
                    GuardarMateria3();

                    LimpiarCuadros();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (cbxAnterior.Checked == true && cbxPosterior.Checked == false)
            {
                try
                {
                    GuardarMateria4();

                    LimpiarCuadros();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }




        }
        private void GuardarMateria()
        {
            _materiasManejador.Guardar(new Entidades.ControlEscolar2.Materias
            {
                Idmateria = txtId.Text,
                Nombremateria = txtMateria.Text, 
                Teoria = int.Parse(txtTeoria.Text),
                Practica = int.Parse(txtPractica.Text),
                Semestre = cmbSemestre.Text,
                MateriaAnterior = cmbAnterior.SelectedValue.ToString(),
                MateriaSiguiente = cmbPosterior.SelectedValue.ToString(),
                Carrera = cmbCarrera.Text



            },lblx.Text);
        }
        private void GuardarMateria2()
        {
            _materiasManejador.Guardar2(new Entidades.ControlEscolar2.Materias
            {
                Idmateria = txtId.Text,
                Nombremateria = txtMateria.Text,
                Teoria = int.Parse(txtTeoria.Text),
                Practica = int.Parse(txtPractica.Text),
                Semestre = cmbSemestre.Text,
                MateriaAnterior = cmbAnterior.SelectedValue.ToString(),
                MateriaSiguiente = cmbPosterior.SelectedValue.ToString(),
                Carrera = cmbCarrera.Text



            }, lblx.Text);
        }
        private void GuardarMateria3()
        {
            _materiasManejador.Guardar3(new Entidades.ControlEscolar2.Materias
            {
                Idmateria = txtId.Text,
                Nombremateria = txtMateria.Text,
                Teoria = int.Parse(txtTeoria.Text),
                Practica = int.Parse(txtPractica.Text),
                Semestre = cmbSemestre.Text,
                MateriaAnterior = cmbAnterior.SelectedValue.ToString(),
                MateriaSiguiente = cmbPosterior.SelectedValue.ToString(),
                Carrera = cmbCarrera.Text



            }, lblx.Text);
        }
        private void GuardarMateria4()
        {
            _materiasManejador.Guardar4(new Entidades.ControlEscolar2.Materias
            {
                Idmateria = txtId.Text,
                Nombremateria = txtMateria.Text,
                Teoria = int.Parse(txtTeoria.Text),
                Practica = int.Parse(txtPractica.Text),
                Semestre = cmbSemestre.Text,
                MateriaAnterior = cmbAnterior.SelectedValue.ToString(),
                MateriaSiguiente = cmbPosterior.SelectedValue.ToString(),
                Carrera = cmbCarrera.Text



            }, lblx.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMateria(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMateria();
                    BuscarMateria("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarMateria()
        {
         
            var idmateria = dgvMaterias.CurrentRow.Cells["Idmateria"].Value;
            _materiasManejador.Eliminar(idmateria.ToString());
           
        }


        private void ModificarMateria()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblx.Text = "1";


            txtId.Text = dgvMaterias.CurrentRow.Cells["Idmateria"].Value.ToString();
            txtMateria.Text = dgvMaterias.CurrentRow.Cells["Nombremateria"].Value.ToString();
            txtTeoria.Text = dgvMaterias.CurrentRow.Cells["Horasteoria"].Value.ToString();
            txtPractica.Text = dgvMaterias.CurrentRow.Cells["Horaspractica"].Value.ToString();
            
            cmbSemestre.Text = dgvMaterias.CurrentRow.Cells["Semestre"].Value.ToString();
            cmbAnterior.Text = dgvMaterias.CurrentRow.Cells["MateriaAnterior"].Value.ToString();
            cmbPosterior.Text = dgvMaterias.CurrentRow.Cells["MateriaPosterior"].Value.ToString();
            cmbCarrera.Text = dgvMaterias.CurrentRow.Cells["Carrera"].Value.ToString();
            


        }

        private void DgvMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                lblx.Text = "1";
                ModificarMateria();
                BuscarMateria("");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
    }
}
