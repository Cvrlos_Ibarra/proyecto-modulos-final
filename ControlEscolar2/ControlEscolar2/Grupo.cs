﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class Grupo : Form
    {
        private AlumnoManejador _alumnoManejador;
        private GrupoManejador _grupomanejador;
        private GruposManejador _gruposManejador;
        private VistaGrupoManejador _vistagrupoManejador;
        private Grupos2Manejador grupos2Manejador;
        public Grupo()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _grupomanejador = new GrupoManejador();
            _gruposManejador = new GruposManejador();
            _vistagrupoManejador = new VistaGrupoManejador();
            grupos2Manejador = new Grupos2Manejador();
        }

        private void Grupo_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
            BuscarGrupo("");
            consultar();
            //ControlarCuadros(false);
            LimpiarCuadros();
            dgvAlumno.Visible = false;
            BuscarGrupo2("");
            txtNumero.Enabled = false;
        }
        private void MostrarGrupos(string filtro)
        {
            cmbGrupo.DataSource = _gruposManejador.GetGrupos(filtro);
            
            
            cmbGrupo.ValueMember = "idgrupos";
            cmbGrupo.DisplayMember = "nombregrupo";
        }

        

        private void CmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarGrupos(cmbCarrera.Text);
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }

        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApp.Enabled = activar;
            txtApm.Enabled = activar;
    
            cmbCarrera.Enabled = activar;
            cmbGrupo.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApp.Text = "";
            txtApm.Text = "";
            
            lblId.Text = "0";
            cmbGrupo.Text = "";
            cmbCarrera.Text = "";
        }
        private void BuscarAlumnos(string filtro)
        {
            dgvAlumno.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }
        private void BuscarGrupo(string filtro)
        {
            //dgvGrupo.DataSource = _grupomanejador.GetGrupo(filtro);
            dgvGrupo.DataSource = _vistagrupoManejador.GetGrupos(filtro);
        }

        private void BuscarGrupo2(string filtro)
        {
            //dgvGrupo.DataSource = _grupomanejador.GetGrupo(filtro);
            dgvGrupos2.DataSource = grupos2Manejador.GetGrupos(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {

                var t = new DataSet();
                t = _grupomanejador.consulta(int.Parse(txtNumero.Text));
                int n = t.Tables[0].Rows.Count;

                if (n == 0)
                {
                    GuardarGrupo();
                    LimpiarCuadros();
                    BuscarAlumnos("");
                }
                else
                {
                    MessageBox.Show("Alumno ya está en un grupo");
                    
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarGrupo()
        {
            _grupomanejador.Guardar(new Entidades.ControlEscolar2.Grupo
            {
                Idg = Convert.ToInt32(lblId.Text),
                Alumno = Convert.ToInt32(txtNumero.Text),
                Grupito = Convert.ToInt32(cmbGrupo.SelectedValue)
            });
        }
        private void GuardarGrupo2()
        {
            grupos2Manejador.Guardar(new Grupos2
            {
                Idgrupo2 = Convert.ToInt32(lblNumero.Text),
                Nombregrupo = txtNombregrupo2.Text,
                Carrera = cmbCarrera2.Text
            });
        }


        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupo(txtBuscar.Text);
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupo();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarGrupo()
        {
            var idg = dgvGrupo.CurrentRow.Cells["id"].Value;
            _grupomanejador.Eliminar(Convert.ToInt32(idg));
        }

        private void EliminarGrupo2()
        {
            
        }

        private void ModificarGrupo()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            
            lblId.Visible = true;
            lblId.Text = dgvGrupo.CurrentRow.Cells["id"].Value.ToString();
            txtNumero.Text = dgvGrupo.CurrentRow.Cells["Numerocontrol"].Value.ToString();
            txtNombre.Text = dgvGrupo.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApp.Text = dgvGrupo.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dgvGrupo.CurrentRow.Cells["apm"].Value.ToString();
            cmbGrupo.Text =dgvGrupo.CurrentRow.Cells["grupo"].Value.ToString();
            cmbCarrera.Text = dgvGrupo.CurrentRow.Cells["carrera"].Value.ToString();
         
        }
        private void ModificarGrupo2()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblId.Visible = true;
            lblId.Text = "0";
            txtNumero.Text = dgvAlumno.CurrentRow.Cells["Numerocontrol"].Value.ToString();
            txtNombre.Text = dgvAlumno.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApp.Text = dgvAlumno.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            txtApm.Text = dgvAlumno.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            cmbGrupo.Text = "";
            cmbCarrera.Text = "";

        }

        private void DgvAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupo2();
                BuscarGrupo("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void DgvGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupo();
                BuscarGrupo("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnGuardar_Click_1(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupo();
                LimpiarCuadros();
                BuscarGrupo("");
                dgvAlumno.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas descartar este alumno del grupo?", "Descartar Alumno", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupo();
                    BuscarGrupo("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            dgvAlumno.Visible = false;
        }

        private void BtnNuevo_Click_1(object sender, EventArgs e)
        {
            dgvAlumno.Visible = true;
        }


        private void consultar()
        {
            
        }

        private void TxtBuscar_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void TabPage1_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar2_Click(object sender, EventArgs e)
        {
            GuardarGrupo2();
        }
    }
}
