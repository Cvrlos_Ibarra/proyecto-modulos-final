﻿namespace ControlEscolar2
{
    partial class Materias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtMateria = new System.Windows.Forms.TextBox();
            this.txtTeoria = new System.Windows.Forms.TextBox();
            this.txtPractica = new System.Windows.Forms.TextBox();
            this.cmbSemestre = new System.Windows.Forms.ComboBox();
            this.cmbAnterior = new System.Windows.Forms.ComboBox();
            this.cmbPosterior = new System.Windows.Forms.ComboBox();
            this.cmbCarrera = new System.Windows.Forms.ComboBox();
            this.dgvMaterias = new System.Windows.Forms.DataGridView();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblx = new System.Windows.Forms.Label();
            this.cbxAnterior = new System.Windows.Forms.CheckBox();
            this.cbxPosterior = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(279, 53);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(340, 20);
            this.txtBuscar.TabIndex = 0;
            this.txtBuscar.TextChanged += new System.EventHandler(this.TxtBuscar_TextChanged);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(60, 144);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 1;
            // 
            // txtMateria
            // 
            this.txtMateria.Location = new System.Drawing.Point(177, 144);
            this.txtMateria.Name = "txtMateria";
            this.txtMateria.Size = new System.Drawing.Size(286, 20);
            this.txtMateria.TabIndex = 2;
            // 
            // txtTeoria
            // 
            this.txtTeoria.Location = new System.Drawing.Point(614, 144);
            this.txtTeoria.Name = "txtTeoria";
            this.txtTeoria.Size = new System.Drawing.Size(67, 20);
            this.txtTeoria.TabIndex = 3;
            // 
            // txtPractica
            // 
            this.txtPractica.Location = new System.Drawing.Point(698, 144);
            this.txtPractica.Name = "txtPractica";
            this.txtPractica.Size = new System.Drawing.Size(67, 20);
            this.txtPractica.TabIndex = 4;
            // 
            // cmbSemestre
            // 
            this.cmbSemestre.FormattingEnabled = true;
            this.cmbSemestre.Items.AddRange(new object[] {
            "1ro",
            "2do",
            "3ro",
            "4to",
            "5to",
            "6to",
            "7mo",
            "8vo",
            "9no"});
            this.cmbSemestre.Location = new System.Drawing.Point(60, 200);
            this.cmbSemestre.Name = "cmbSemestre";
            this.cmbSemestre.Size = new System.Drawing.Size(76, 21);
            this.cmbSemestre.TabIndex = 5;
            // 
            // cmbAnterior
            // 
            this.cmbAnterior.FormattingEnabled = true;
            this.cmbAnterior.Location = new System.Drawing.Point(159, 200);
            this.cmbAnterior.Name = "cmbAnterior";
            this.cmbAnterior.Size = new System.Drawing.Size(232, 21);
            this.cmbAnterior.TabIndex = 6;
            // 
            // cmbPosterior
            // 
            this.cmbPosterior.FormattingEnabled = true;
            this.cmbPosterior.Location = new System.Drawing.Point(397, 200);
            this.cmbPosterior.Name = "cmbPosterior";
            this.cmbPosterior.Size = new System.Drawing.Size(232, 21);
            this.cmbPosterior.TabIndex = 7;
            // 
            // cmbCarrera
            // 
            this.cmbCarrera.FormattingEnabled = true;
            this.cmbCarrera.Items.AddRange(new object[] {
            "ISC",
            "ISA",
            "II",
            "IGE",
            "IC",
            "IE"});
            this.cmbCarrera.Location = new System.Drawing.Point(644, 200);
            this.cmbCarrera.Name = "cmbCarrera";
            this.cmbCarrera.Size = new System.Drawing.Size(121, 21);
            this.cmbCarrera.TabIndex = 8;
            // 
            // dgvMaterias
            // 
            this.dgvMaterias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterias.Location = new System.Drawing.Point(60, 316);
            this.dgvMaterias.Name = "dgvMaterias";
            this.dgvMaterias.Size = new System.Drawing.Size(705, 182);
            this.dgvMaterias.TabIndex = 9;
            this.dgvMaterias.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvMaterias_CellDoubleClick);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.PapayaWhip;
            this.btnEliminar.BackgroundImage = global::ControlEscolar2.Properties.Resources.trash;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(718, 506);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(47, 48);
            this.btnEliminar.TabIndex = 59;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.BackgroundImage = global::ControlEscolar2.Properties.Resources._return;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(115, 506);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(46, 48);
            this.btnCancelar.TabIndex = 58;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnGuardar.BackgroundImage = global::ControlEscolar2.Properties.Resources.save;
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(60, 506);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(49, 48);
            this.btnGuardar.TabIndex = 57;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.Turquoise;
            this.btnNuevo.BackgroundImage = global::ControlEscolar2.Properties.Resources.proficon;
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevo.Location = new System.Drawing.Point(60, 262);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(49, 48);
            this.btnNuevo.TabIndex = 55;
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(57, 128);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(18, 13);
            this.lblId.TabIndex = 60;
            this.lblId.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Materia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(611, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 62;
            this.label2.Text = "Horas Teoría";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(695, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 63;
            this.label3.Text = "Horas Práctica";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Semestre";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Materia Anterior";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(394, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "Materia Posterior";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(646, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Carrera";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::ControlEscolar2.Properties.Resources.icons8_google_web_search_52;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(251, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 21);
            this.pictureBox1.TabIndex = 68;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(367, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 22);
            this.label8.TabIndex = 69;
            this.label8.Text = "MATERIAS";
            // 
            // lblx
            // 
            this.lblx.AutoSize = true;
            this.lblx.Location = new System.Drawing.Point(57, 61);
            this.lblx.Name = "lblx";
            this.lblx.Size = new System.Drawing.Size(13, 13);
            this.lblx.TabIndex = 70;
            this.lblx.Text = "0";
            // 
            // cbxAnterior
            // 
            this.cbxAnterior.AutoSize = true;
            this.cbxAnterior.Location = new System.Drawing.Point(243, 183);
            this.cbxAnterior.Name = "cbxAnterior";
            this.cbxAnterior.Size = new System.Drawing.Size(15, 14);
            this.cbxAnterior.TabIndex = 73;
            this.cbxAnterior.UseVisualStyleBackColor = true;
            // 
            // cbxPosterior
            // 
            this.cbxPosterior.AutoSize = true;
            this.cbxPosterior.Location = new System.Drawing.Point(486, 183);
            this.cbxPosterior.Name = "cbxPosterior";
            this.cbxPosterior.Size = new System.Drawing.Size(15, 14);
            this.cbxPosterior.TabIndex = 74;
            this.cbxPosterior.UseVisualStyleBackColor = true;
            // 
            // Materias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(827, 578);
            this.Controls.Add(this.cbxPosterior);
            this.Controls.Add(this.cbxAnterior);
            this.Controls.Add(this.lblx);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.dgvMaterias);
            this.Controls.Add(this.cmbCarrera);
            this.Controls.Add(this.cmbPosterior);
            this.Controls.Add(this.cmbAnterior);
            this.Controls.Add(this.cmbSemestre);
            this.Controls.Add(this.txtPractica);
            this.Controls.Add(this.txtTeoria);
            this.Controls.Add(this.txtMateria);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.txtBuscar);
            this.Name = "Materias";
            this.Text = "Materias";
            this.Load += new System.EventHandler(this.Materias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtMateria;
        private System.Windows.Forms.TextBox txtTeoria;
        private System.Windows.Forms.TextBox txtPractica;
        private System.Windows.Forms.ComboBox cmbSemestre;
        private System.Windows.Forms.ComboBox cmbAnterior;
        private System.Windows.Forms.ComboBox cmbPosterior;
        private System.Windows.Forms.ComboBox cmbCarrera;
        private System.Windows.Forms.DataGridView dgvMaterias;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblx;
        private System.Windows.Forms.CheckBox cbxAnterior;
        private System.Windows.Forms.CheckBox cbxPosterior;
    }
}