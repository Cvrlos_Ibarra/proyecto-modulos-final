﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Asignacion
    {
        private int _idasignacion;
        private string _fkprofesor;
        private string _fkmateria;
        private int _fkgrupos;

        public int Idasignacion { get => _idasignacion; set => _idasignacion = value; }
        public string Fkprofesor { get => _fkprofesor; set => _fkprofesor = value; }
        public string Fkmateria { get => _fkmateria; set => _fkmateria = value; }
        public int Fkgrupos { get => _fkgrupos; set => _fkgrupos = value; }
    }
}
