﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estados
    {
        private string _codigoestado;
        private string _estado;

        public string Codigoestado { get => _codigoestado; set => _codigoestado = value; }
        public string Estado { get => _estado; set => _estado = value; }
    }
}
