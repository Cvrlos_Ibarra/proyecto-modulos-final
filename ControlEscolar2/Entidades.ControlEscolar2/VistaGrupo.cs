﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaGrupo
    {
        private int _numerocontrol;
        private string nombre;
        private string _app;
        private string _apm;
        private string _grupo;
        private string _carrera;
        private string _id;

        public int Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Id { get => _id; set => _id = value; }
    }
}
