﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupos
    {
        private int _idgrupos;
        private string _nombregrupo;
        private string _carrera;

        public int Idgrupos { get => _idgrupos; set => _idgrupos = value; }
        public string Nombregrupo { get => _nombregrupo; set => _nombregrupo = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
