﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estudios
    {
        private int _codigoest;
        private string _estudio;
        private string _documento;
        private string _fkprofesor;

        public int Codigoest { get => _codigoest; set => _codigoest = value; }
        public string Estudio { get => _estudio; set => _estudio = value; }
        public string Documento { get => _documento; set => _documento = value; }
        public string Fkprofesor { get => _fkprofesor; set => _fkprofesor = value; }
    }
}
