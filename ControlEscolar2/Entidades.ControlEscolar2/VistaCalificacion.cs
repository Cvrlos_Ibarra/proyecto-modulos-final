﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaCalificacion
    {
        private string _carrera;
        private string _grupo;
        private string _materia;
        private int _numerocontrol;
        private string _nombre;
        private string _app;
        private string _apm;
        private int _parcial1;
        private int _parcial2;
        private int _parcial3;
        private int _parcial4;
        private int _id;
        private int _idgrupo;

        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public int Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public int Parcial1 { get => _parcial1; set => _parcial1 = value; }
        public int Parcial2 { get => _parcial2; set => _parcial2 = value; }
        public int Parcial3 { get => _parcial3; set => _parcial3 = value; }
        public int Parcial4 { get => _parcial4; set => _parcial4 = value; }
        public int Id { get => _id; set => _id = value; }
        public int Idgrupo { get => _idgrupo; set => _idgrupo = value; }
    }
}
