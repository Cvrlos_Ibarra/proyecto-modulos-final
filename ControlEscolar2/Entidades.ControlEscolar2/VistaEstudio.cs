﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaEstudio
    {
        private int _clave;
        private string _ncontrol;
        private string _nombre;
        private string _apellidop;
        private string _apellidom;
        private int _cedula;
        private string _titulo;
        private string _tituloextra;
        private string _documento;

        public int Clave { get => _clave; set => _clave = value; }
        public string Ncontrol { get => _ncontrol; set => _ncontrol = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidop { get => _apellidop; set => _apellidop = value; }
        public string Apellidom { get => _apellidom; set => _apellidom = value; }
        public int Cedula { get => _cedula; set => _cedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Tituloextra { get => _tituloextra; set => _tituloextra = value; }
        public string Documento { get => _documento; set => _documento = value; }
    }
}
