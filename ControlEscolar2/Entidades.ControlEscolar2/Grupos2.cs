﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupos2
    {
        private int _idgrupo2;
        private string _nombregrupo;
        private string _carrera;

        public int Idgrupo2 { get => _idgrupo2; set => _idgrupo2 = value; }
        public string Nombregrupo { get => _nombregrupo; set => _nombregrupo = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
