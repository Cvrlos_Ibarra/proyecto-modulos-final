﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaAsignacion
    {
        private int _id;
        private string _grupo;
        private string _carrera;
        private string _materia;
        private string _numerocontrol;
        private string _profesor;
        private string _app;
        private string _apm;

        public int Id { get => _id; set => _id = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public string Profesor { get => _profesor; set => _profesor = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
    }
}
