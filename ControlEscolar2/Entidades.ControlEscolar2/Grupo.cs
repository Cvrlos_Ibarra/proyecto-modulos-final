﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupo
    {
        private int _idg;
        private int _alumno;
        private int _grupito;

        public int Idg { get => _idg; set => _idg = value; }
        public int Alumno { get => _alumno; set => _alumno = value; }
        public int Grupito { get => _grupito; set => _grupito = value; }
    }
}
