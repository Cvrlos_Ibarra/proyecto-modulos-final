﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Text.RegularExpressions;

namespace LogicaNegocios.ControlEscolar2
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _usuarioAccesoDatos = new UsuarioAccesoDatos();

        private bool NombreValido(string nombre)
        {

            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;

        }


        public Tuple<bool,string> ValidarUsuario(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;

            

            if(usuario.Nombre.Length == 0)
            {
                mensaje = mensaje + "El nombre de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.Nombre.Length > 20)
            {
                mensaje = mensaje + "El nombre de usuario solo permite un máximo de 20 caracteres \n";
                valido = false;
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = mensaje + "Esccribe un formato valido para el nombre del usuario \n";
                valido = false;
            }
            if (usuario.App.Length == 0)
            {
                mensaje = mensaje + "El Apellido paterno de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.App.Length > 20)
            {
                mensaje = mensaje + "El Apellido paterno de usuario solo permite un máximo de 20 caracteres \n";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }

        public void Guardar(Usuario usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);

        }
        public void Eliminar(int idusuario)
        {
            //eliminar
            _usuarioAccesoDatos.Eliminar(idusuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = _usuarioAccesoDatos.GetUsuarios(filtro);
            //Llenar lista
            return listUsuario;
        }

    }
}
