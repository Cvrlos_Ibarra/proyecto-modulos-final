﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class CalificacionManejador
    {
        private CalificacionAccesoDatos _calificacionAccesoDatos = new CalificacionAccesoDatos();
        public void Guardar(Calificacion calificacion)
        {
            _calificacionAccesoDatos.Guardar(calificacion);

        }
        public void Eliminar(int idcalificacion)
        {
            //eliminar
            _calificacionAccesoDatos.Eliminar(idcalificacion);
        }
        public List<Calificacion> GetCalificacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listCalificacion = _calificacionAccesoDatos.GetCalificacion(filtro);
            //Llenar lista
            return listCalificacion;
        }




        public DataSet consulta(int filtro)
        {
            DataSet dt = _calificacionAccesoDatos.Consulta(filtro);

            return dt;
        }
    }
}
