﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class GruposManejador
    {
        private GruposAccesoDatos _gruposAccesoDatos = new GruposAccesoDatos();
        public List<Grupos> GetGrupos(string filtro)
        {
            var listGrupos = _gruposAccesoDatos.GetGrupos(filtro);

            return listGrupos;

        }
    }
}
