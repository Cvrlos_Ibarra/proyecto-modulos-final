﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _materiaAccesoDatos = new MateriasAccesoDatos();
        public void Guardar(Materias materia, string x)
        {

            _materiaAccesoDatos.Guardar(materia, x);

        }
        public void Guardar2(Materias materia, string x)
        {

            _materiaAccesoDatos.Guardar2(materia, x);

        }
        public void Guardar3(Materias materia, string x)
        {

            _materiaAccesoDatos.Guardar3(materia, x);

        }
        public void Guardar4(Materias materia, string x)
        {

            _materiaAccesoDatos.Guardar4(materia, x);

        }

        public void Eliminar(string id)
        {
            //eliminar
            _materiaAccesoDatos.Eliminar(id);
        }

        public List<Materias> GetMaterias(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listMaterias = _materiaAccesoDatos.GetMaterias(filtro);
            //Llenar lista
            return listMaterias;
        }
    }
}
