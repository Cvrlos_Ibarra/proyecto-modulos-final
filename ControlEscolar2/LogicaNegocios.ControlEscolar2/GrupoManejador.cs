﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class GrupoManejador
    {
        private GrupoAccesoDatos _grupoAccesoDatos = new GrupoAccesoDatos();
        public void Guardar(Grupo grupo)
        {
            _grupoAccesoDatos.Guardar(grupo);

        }
        public void Eliminar(int idgrupo)
        {
            //eliminar
            _grupoAccesoDatos.Eliminar(idgrupo);
        }
        public List<Grupo> GetGrupo(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupo = _grupoAccesoDatos.GetGrupo(filtro);
            //Llenar lista
            return listGrupo;
        }


        public List<Alumno> GetAlumnos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAlumno = _grupoAccesoDatos.GetAlumnos(filtro);
            //Llenar lista
            return listAlumno;
        }

        public DataSet consulta(int filtro)
        {
            DataSet dt = _grupoAccesoDatos.Consulta(filtro);

            return dt;
        }
    }
}
