﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();
        public void Guardar(Profesor profesor, string x)
        {
            
            _profesorAccesoDatos.Guardar(profesor,x);

        }
        public void Eliminar(string numerocontrol)
        {
            //eliminar
            _profesorAccesoDatos.Eliminar(numerocontrol);
        }
        
        public List<Profesor> GetProfesores(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listProfesor = _profesorAccesoDatos.GetProfesores(filtro);
            //Llenar lista
            return listProfesor;
        }
        /*public List<Profesor> GetIds(string filtro)
        {
            var listId = _profesorAccesoDatos.GetId(filtro);

            return listId;
        }*/
    }
}
