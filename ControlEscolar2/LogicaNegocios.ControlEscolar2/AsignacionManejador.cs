﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class AsignacionManejador
    {
        private AsignacionAccesoDatos _asignacionAccesoDatos = new AsignacionAccesoDatos();
        public void Guardar(Asignacion asignacion)
        {
            _asignacionAccesoDatos.Guardar(asignacion);

        }
        public void Eliminar(int idasignacion)
        {
            //eliminar
            _asignacionAccesoDatos.Eliminar(idasignacion);
        }
        public List<Asignacion> GetAsignacion(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAsignacion = _asignacionAccesoDatos.GetAsignacion(filtro);
            //Llenar lista
            return listAsignacion;
        }


       

        public DataSet consulta(int filtro)
        {
            DataSet dt = _asignacionAccesoDatos.Consulta(filtro);

            return dt;
        }
    }
}
