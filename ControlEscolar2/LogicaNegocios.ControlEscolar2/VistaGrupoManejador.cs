﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaGrupoManejador
    {
        private VistaGrupoAccesoDatos _vistaGrupoManejador = new VistaGrupoAccesoDatos();

        public List<VistaGrupo> GetGrupos(string filtro)
        {

            var listVistaGrupo = _vistaGrupoManejador.GetGrupos(filtro);

            return listVistaGrupo;
        }
    }
}
