﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaAsignacionManejador
    {
        private VistaAsignacionAccesoDatos _vistaasignacion = new VistaAsignacionAccesoDatos();

        public List<VistaAsignacion> GetAsignacion(string filtro)
        {

            var listVistaAsignacion = _vistaasignacion.GetAsignacion(filtro);

            return listVistaAsignacion;
        }
    }
}
