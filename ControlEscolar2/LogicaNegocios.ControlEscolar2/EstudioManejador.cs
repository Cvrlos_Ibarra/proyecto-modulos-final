﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class EstudioManejador
    {
        private EstudioAccesoDatos _estudioAccesoDatos = new EstudioAccesoDatos();
        public void Guardar(Estudios estudios)
        {
            _estudioAccesoDatos.Guardar(estudios);

        }
        public void Eliminar(int codigoestudio)
        {
            //eliminar
            _estudioAccesoDatos.Eliminar(codigoestudio);
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEstudios = _estudioAccesoDatos.GetEstudios(filtro);
            //Llenar lista
            return listEstudios;
        }
    }
}
