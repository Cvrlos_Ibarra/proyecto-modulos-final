﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class Grupos2Manejador
    {
        private Grupos2AccesoDatos _grupo2AccesoDatos = new Grupos2AccesoDatos();
        public void Guardar(Grupos2 grupos)
        {
            _grupo2AccesoDatos.Guardar(grupos);

        }
        public void Eliminar(int idgrupo)
        {
            //eliminar
            _grupo2AccesoDatos.Eliminar(idgrupo);
        }
        public List<Grupos2> GetGrupos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupos = _grupo2AccesoDatos.GetGrupo(filtro);
            //Llenar lista
            return listGrupos;
        }
    }
}
