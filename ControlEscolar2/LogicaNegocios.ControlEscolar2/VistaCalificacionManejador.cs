﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaCalificacionManejador
    {
        private VistaCalificacionAccesoDatos _vistaCalificacion = new VistaCalificacionAccesoDatos();

        public List<VistaCalificacion> GetCalificacion(string filtro, string filtro2)
        {

            var listVistaCalificacion = _vistaCalificacion.GetCalificacion(filtro, filtro2);

            return listVistaCalificacion;
        }
    }
}
