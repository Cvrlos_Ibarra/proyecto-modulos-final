﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class MunicipiosManejador
    {
        private MunicipioAccesoDatos _municipioAccesoDatos = new MunicipioAccesoDatos();
        public List<Municipios> GetMunicipios(string filtro)
        {
            var listMunicipios = _municipioAccesoDatos.GetMunicipios(filtro);

            return listMunicipios;

        }
    }
}
